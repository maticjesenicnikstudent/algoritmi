//============================================================================
// Name        : algoritmi.cpp
// Author      : Matic Jesenicnik
// Version     :
// Description : Program za prikaz treh preprostih algoritmov
//============================================================================

#include <iostream>
#include <stdio.h>
#include <math.h>
#include <vector>
using namespace std;

char menu();
void izpisPrastevilskihProduktov(int vnos);
vector<int> iskanjePrastevil(int vnos);
vector<int> prastevilskiProdukti(vector<int> prastevila, int vnos);
void izpisVektorja(vector<int> vektor);
int korenCelegaStevila(int vnos);
void optimalnaStrategijaIgra(vector<int> vektorStevil);
vector<int> vnosOptimalnaStrategija();

int main() {
	bool running = true;
	int vnos;
	char izbira;

	do {
		izbira = menu();
		switch (izbira) {
		case '1':
			cout << "Vnesi stevilo za iskanje korena\n";
			cin >> vnos;
			cout << korenCelegaStevila(vnos) << endl;
			break;
		case '2':
			cout << "Vnesi stevilo za iskanje prastevilskih produktov\n";
			cin >> vnos;
			izpisPrastevilskihProduktov(vnos);
			break;
		case '3':
			optimalnaStrategijaIgra(vnosOptimalnaStrategija());
			break;
		case '4':
			running = false;
			break;
		default:
			cout << "Wrong selection!\n";
		}
	} while (running);
	return 0;
}

char menu(){

	cout << "\nAlgoritmi - izbira:\n";
	cout << "1 Iskanje korena celega stevila\n";
	cout << "2 Izpis vseh prastevilskih produktov celega stevila\n";
	cout << "3 Optimalna strategija za igro\n";
	cout << "4 Konec\n";
	cout << "=========\n";
	cout << "Vasa izbira: ";

	string selection;
	do
	{
		if (!getline(cin, selection))
		{
			return 0;
		}
	} while (selection.length() == 0);
	return selection[0];
}

vector<int> iskanjePrastevil(int vnos){
	bool isItPrastevilo;
	vector<int> prastevila;
	prastevila.push_back(2);
	prastevila.push_back(3);

	for(int i = 3; i < vnos; i += 2){
		for(int j = 0; j < int(prastevila.size()) && sqrt(i) > j; j++){
			if(i % prastevila[j] == 0){
				isItPrastevilo = false;
				break;
				}
			}
		if(isItPrastevilo == true){
			prastevila.push_back(i);
			}
		isItPrastevilo = true;
	}

	return prastevila;
}

vector<int> prastevilskiProdukti(vector<int> prastevila, int vnos){
	vector<int> produkti;
	int i = 0;
	while(vnos != 1){
		if(vnos % prastevila[i] == 0){
			produkti.push_back(prastevila[i]);
			vnos /= prastevila[i];
		} else {
			i++;
		}
	}
	return produkti;
}

void izpisVektorja(vector<int> vektor){
	for(int i = 0; i < int(vektor.size()); i++){
		cout << vektor[i] << '\t';
	}
	cout << endl;
}

void izpisPrastevilskihProduktov(int vnos){
	vector<int> prastevila = iskanjePrastevil(vnos);
	vector<int> produkti = prastevilskiProdukti(prastevila, vnos);
	izpisVektorja(produkti);
}

int korenCelegaStevila(int vnos){
	int i = 1;
	if(vnos == 1 || vnos == 0){
		return vnos;
	}
	while(i*i <= vnos){
		i++;
	}
	return i-1;
}

vector<int> vnosOptimalnaStrategija(){
	int st, vnos;
	vector<int> vektorStevil;
	do{
	cout << "Koliko stevil zelite vnesti (sodo stevilo)\n";
	cin >> st;
	if(st%2!=0){
		cout << "Prosim vnesite sodo stevilo\n";
	}
	}while(st%2!=0);
	for(int i = 0; i < st; i++){
		cout << "Vnesite " << i+1 << ". stevilo: ";
		cin >> vnos; vektorStevil.push_back(vnos);
	}

	return vektorStevil;
}

void optimalnaStrategijaIgra(vector<int> vektorStevil){
	int n = int(vektorStevil.size());
	int table[n][n];

	for (int gap = 0; gap < n; ++gap) {
		for (int i = 0, j = gap; j < n; ++i, ++j) {
			int x = ((i + 2) <= j) ? table[i + 2][j] : 0;
			int y = ((i + 1) <= (j - 1)) ? table[i + 1][j - 1] : 0;
			int z = (i <= (j - 2)) ? table[i][j - 2] : 0;
			table[i][j] = max(vektorStevil[i] + min(x, y), vektorStevil[j] + min(y, z));
		}
	}

	cout << "Najboljsi mozni rezultat je: " << table[0][n - 1] << ".\n";
}
